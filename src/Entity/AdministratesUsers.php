<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdministratesUsers
 *
 * @ORM\Table(name="administrates_users", uniqueConstraints={@ORM\UniqueConstraint(name="uk_administrates_users", columns={"user_id", "invited_user_id", "to_company_id"})}, indexes={@ORM\Index(name="fk_administrates_users_user_id_idx", columns={"user_id"}), @ORM\Index(name="fk_administrates_users_to_company_id_idx", columns={"to_company_id"}), @ORM\Index(name="fk_administrates_users_invited_user_id_idx", columns={"invited_user_id"})})
 * @ORM\Entity
 */
class AdministratesUsers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="with_roles", type="text", length=65535, nullable=false)
     */
    private $withRoles;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invited_user_id", referencedColumnName="id")
     * })
     */
    private $invitedUser;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="to_company_id", referencedColumnName="id")
     * })
     */
    private $toCompany;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWithRoles(): ?string
    {
        return $this->withRoles;
    }

    public function setWithRoles(string $withRoles): self
    {
        $this->withRoles = $withRoles;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getInvitedUser(): ?Users
    {
        return $this->invitedUser;
    }

    public function setInvitedUser(?Users $invitedUser): self
    {
        $this->invitedUser = $invitedUser;

        return $this;
    }

    public function getToCompany(): ?Company
    {
        return $this->toCompany;
    }

    public function setToCompany(?Company $toCompany): self
    {
        $this->toCompany = $toCompany;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }


}
