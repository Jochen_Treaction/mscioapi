<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * Campaign
 * @ApiResource(attributes={"pagination_enabled"=false})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact", "companyId": "exact", "objectRegisterId":"exact", "surveyId": "exact","status":"exact","usecaseType":"exact"})
 * @ORM\Table(name="campaign", uniqueConstraints={@ORM\UniqueConstraint(name="uk_campaign_name_version_status_compid", columns={"name", "version", "status", "company_id"})}, indexes={@ORM\Index(name="ff_campaign_survey_idx", columns={"survey_id"}), @ORM\Index(name="uk_campaign_account_no", columns={"account_no"}), @ORM\Index(name="company_id", columns={"company_id"}), @ORM\Index(name="idx_campaign_name", columns={"name"}), @ORM\Index(name="fk_objectregister_idx", columns={"objectregister_id"})})
 * @ORM\Entity
 */
class Campaign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="usecase_type", type="string", length=40, nullable=false, options={"default"="campaign"})
     */
    private $usecaseType = 'campaign';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="archivedate", type="datetime", nullable=true)
     */
    private $archivedate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="activationdate", type="datetime", nullable=true)
     */
    private $activationdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="account_no", type="string", length=40, nullable=true)
     */
    private $accountNo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="activationby", type="integer", nullable=false)
     */
    private $activationby;

    /**
     * @var int
     *
     * @ORM\Column(name="archiveby", type="integer", nullable=false)
     */
    private $archiveby;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=40, nullable=false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=false, options={"default"="draft"})
     */
    private $status = 'draft';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsecaseType(): ?string
    {
        return $this->usecaseType;
    }

    public function setUsecaseType(string $usecaseType): self
    {
        $this->usecaseType = $usecaseType;

        return $this;
    }

    public function getArchivedate(): ?\DateTimeInterface
    {
        return $this->archivedate;
    }

    public function setArchivedate(?\DateTimeInterface $archivedate): self
    {
        $this->archivedate = $archivedate;

        return $this;
    }

    public function getActivationdate(): ?\DateTimeInterface
    {
        return $this->activationdate;
    }

    public function setActivationdate(?\DateTimeInterface $activationdate): self
    {
        $this->activationdate = $activationdate;

        return $this;
    }

    public function getAccountNo(): ?string
    {
        return $this->accountNo;
    }

    public function setAccountNo(?string $accountNo): self
    {
        $this->accountNo = $accountNo;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getActivationby(): ?int
    {
        return $this->activationby;
    }

    public function setActivationby(int $activationby): self
    {
        $this->activationby = $activationby;

        return $this;
    }

    public function getArchiveby(): ?int
    {
        return $this->archiveby;
    }

    public function setArchiveby(int $archiveby): self
    {
        $this->archiveby = $archiveby;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }


}
