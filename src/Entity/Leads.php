<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Leads
 * @ApiResource(attributes={"pagination_enabled"=false})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact","campaign_id": "exact" , "objectregister_id": "exact","campaign": "exact", "account": "exact", "email": "partial", "ip": "exact","url": "partial", "created": "partial"})
 * @ORM\Table(name="leads", indexes={@ORM\Index(name="account", columns={"account"}), @ORM\Index(name="campaign_id", columns={"campaign_id"}), @ORM\Index(name="fk_obj_reg_idx", columns={"objectregister_id"})})
 * @ORM\Entity
 */
class Leads
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="campaign_id", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $campaignId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="salutation", type="string", length=255, nullable=false)
     */
    private $salutation = '';

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="firma", type="string", length=255, nullable=false)
     */
    private $firma = '';

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     */
    private $street = '';

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=false)
     */
    private $postalCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = '';

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country = '';

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     */
    private $phone = '';

    /**
     * @var int
     *
     * @ORM\Column(name="account", type="integer", nullable=false)
     */
    private $account = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="final_url", type="string", length=255, nullable=false)
     */
    private $finalUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="trafficsource", type="string", length=255, nullable=false)
     */
    private $trafficsource = '';

    /**
     * @var string
     *
     * @ORM\Column(name="utm_parameters", type="string", length=255, nullable=false)
     */
    private $utmParameters = '';

    /**
     * @var string
     *
     * @ORM\Column(name="campaign", type="string", length=255, nullable=false)
     */
    private $campaign = '';

    /**
     * @var string
     *
     * @ORM\Column(name="split_version", type="string", length=255, nullable=false)
     */
    private $splitVersion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lead_reference", type="string", length=255, nullable=false)
     */
    private $leadReference = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="targetgroup", type="string", length=100, nullable=true)
     */
    private $targetgroup = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="affiliateID", type="string", length=255, nullable=true, options={"comment"="Affiliate Id from campaign URL"})
     */
    private $affiliateid = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="affiliateSubID", type="string", length=255, nullable=true, options={"comment"="AffiliateSubId from campaign URL"})
     */
    private $affiliatesubid = '';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=false, options={"default"="active"})
     */
    private $status = 'active';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100, nullable=false)
     */
    private $ip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string", length=100, nullable=false)
     */
    private $deviceType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="device_browser", type="string", length=100, nullable=false)
     */
    private $deviceBrowser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="device_os", type="string", length=100, nullable=false)
     */
    private $deviceOs = '';

    /**
     * @var string
     *
     * @ORM\Column(name="device_os_version", type="string", length=100, nullable=false)
     */
    private $deviceOsVersion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="device_browser_version", type="string", length=100, nullable=false)
     */
    private $deviceBrowserVersion = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var int
     *
     * @ORM\Column(name="modification", type="integer", nullable=false)
     */
    private $modification;

    /**
     * @var int
     *
     * @ORM\Column(name="status_reason", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $statusReason;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCampaignId(): ?string
    {
        return $this->campaignId;
    }

    public function setCampaignId(string $campaignId): self
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSalutation(): ?string
    {
        return $this->salutation;
    }

    public function setSalutation(string $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirma(): ?string
    {
        return $this->firma;
    }

    public function setFirma(string $firma): self
    {
        $this->firma = $firma;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAccount(): ?int
    {
        return $this->account;
    }

    public function setAccount(int $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getFinalUrl(): ?string
    {
        return $this->finalUrl;
    }

    public function setFinalUrl(string $finalUrl): self
    {
        $this->finalUrl = $finalUrl;

        return $this;
    }

    public function getTrafficsource(): ?string
    {
        return $this->trafficsource;
    }

    public function setTrafficsource(string $trafficsource): self
    {
        $this->trafficsource = $trafficsource;

        return $this;
    }

    public function getUtmParameters(): ?string
    {
        return $this->utmParameters;
    }

    public function setUtmParameters(string $utmParameters): self
    {
        $this->utmParameters = $utmParameters;

        return $this;
    }

    public function getCampaign(): ?string
    {
        return $this->campaign;
    }

    public function setCampaign(string $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getSplitVersion(): ?string
    {
        return $this->splitVersion;
    }

    public function setSplitVersion(string $splitVersion): self
    {
        $this->splitVersion = $splitVersion;

        return $this;
    }

    public function getLeadReference(): ?string
    {
        return $this->leadReference;
    }

    public function setLeadReference(string $leadReference): self
    {
        $this->leadReference = $leadReference;

        return $this;
    }

    public function getTargetgroup(): ?string
    {
        return $this->targetgroup;
    }

    public function setTargetgroup(?string $targetgroup): self
    {
        $this->targetgroup = $targetgroup;

        return $this;
    }

    public function getAffiliateid(): ?string
    {
        return $this->affiliateid;
    }

    public function setAffiliateid(?string $affiliateid): self
    {
        $this->affiliateid = $affiliateid;

        return $this;
    }

    public function getAffiliatesubid(): ?string
    {
        return $this->affiliatesubid;
    }

    public function setAffiliatesubid(?string $affiliatesubid): self
    {
        $this->affiliatesubid = $affiliatesubid;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDeviceType(): ?string
    {
        return $this->deviceType;
    }

    public function setDeviceType(string $deviceType): self
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    public function getDeviceBrowser(): ?string
    {
        return $this->deviceBrowser;
    }

    public function setDeviceBrowser(string $deviceBrowser): self
    {
        $this->deviceBrowser = $deviceBrowser;

        return $this;
    }

    public function getDeviceOs(): ?string
    {
        return $this->deviceOs;
    }

    public function setDeviceOs(string $deviceOs): self
    {
        $this->deviceOs = $deviceOs;

        return $this;
    }

    public function getDeviceOsVersion(): ?string
    {
        return $this->deviceOsVersion;
    }

    public function setDeviceOsVersion(string $deviceOsVersion): self
    {
        $this->deviceOsVersion = $deviceOsVersion;

        return $this;
    }

    public function getDeviceBrowserVersion(): ?string
    {
        return $this->deviceBrowserVersion;
    }

    public function setDeviceBrowserVersion(string $deviceBrowserVersion): self
    {
        $this->deviceBrowserVersion = $deviceBrowserVersion;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getModification(): ?int
    {
        return $this->modification;
    }

    public function setModification(int $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getStatusReason(): ?string
    {
        return $this->statusReason;
    }

    public function setStatusReason(string $statusReason): self
    {
        $this->statusReason = $statusReason;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }


}
