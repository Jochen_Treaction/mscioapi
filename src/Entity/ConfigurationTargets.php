<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationTargets
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact", "configuration_groups.name": "exact"})
 * @ORM\Table(name="configuration_targets", uniqueConstraints={@ORM\UniqueConstraint(name="uk_target_group", columns={"id", "configuration_group_id"}), @ORM\UniqueConstraint(name="uk_target_name", columns={"name"})}, indexes={@ORM\Index(name="fk_target_group_idx", columns={"configuration_group_id"})})
 * @ORM\Entity
 */
class ConfigurationTargets
{
    /**
     * @var int
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name = '';

    /**
     * @var string|null
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="desc", type="text", length=16777215, nullable=true)
     */
    private $desc;

    /**
     * @var \DateTime
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="updated", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     * @Groups({"configuration_groups"})
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \ConfigurationGroups
     * @Groups({"configuration_groups"})
     * @ORM\ManyToOne(targetEntity="ConfigurationGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="configuration_group_id", referencedColumnName="id")
     * })
     */
    private $configurationGroup;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDesc(): ?string
    {
        return $this->desc;
    }

    public function setDesc(?string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getConfigurationGroup(): ?ConfigurationGroups
    {
        return $this->configurationGroup;
    }

    public function setConfigurationGroup(?ConfigurationGroups $configurationGroup): self
    {
        $this->configurationGroup = $configurationGroup;

        return $this;
    }


}
