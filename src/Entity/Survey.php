<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * Survey
 * @ApiResource(attributes={"pagination_enabled"=false})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact"})
 * @ORM\Table(name="survey", indexes={@ORM\Index(name="fk_objectregister_sy_idx", columns={"objectregister_id"}), @ORM\Index(name="survey_status1_idx", columns={"layout_type_id"})})
 * @ORM\Entity
 */
class Survey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="survey_is_online", type="boolean", nullable=false, options={"comment"="flag to turn survey online (1)  or offline (0). default is on ( 1 )\n"})
     */
    private $surveyIsOnline = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="survey_description", type="text", length=65535, nullable=true, options={"comment"="description of survey, add customer requirements or comments of the survey-maker here. add textarea to frontend."})
     */
    private $surveyDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled cio.users.id"})
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled cio.users.id"})
     */
    private $updatedBy;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

    /**
     * @var \LayoutType
     *
     * @ORM\ManyToOne(targetEntity="LayoutType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layout_type_id", referencedColumnName="id")
     * })
     */
    private $layoutType;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSurveyIsOnline(): ?bool
    {
        return $this->surveyIsOnline;
    }

    public function setSurveyIsOnline(bool $surveyIsOnline): self
    {
        $this->surveyIsOnline = $surveyIsOnline;

        return $this;
    }

    public function getSurveyDescription(): ?string
    {
        return $this->surveyDescription;
    }

    public function setSurveyDescription(?string $surveyDescription): self
    {
        $this->surveyDescription = $surveyDescription;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }

    public function getLayoutType(): ?LayoutType
    {
        return $this->layoutType;
    }

    public function setLayoutType(?LayoutType $layoutType): self
    {
        $this->layoutType = $layoutType;

        return $this;
    }


}
