<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * Integrations
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "companyId": "exact", "configurationTargetId": "exact"})
 * @ORM\Table(name="integrations", indexes={@ORM\Index(name="fk_integrations_company_idx", columns={"company_id"}), @ORM\Index(name="fk_campaign_idx", columns={"campaign_id"}), @ORM\Index(name="fk_integrations_config_targets_idx", columns={"configuration_target_id"})})
 * @ORM\Entity
 */
class Integrations
{
    /**
     * @var int
     * @Groups({"integrations"})
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     * @Groups({"integrations"})
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     * @Groups({"integrations"})
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime
     * @Groups({"integrations"})
     * @ORM\Column(name="updated", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     * @Groups({"integrations"})
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Campaign
     * @Groups({"integrations"})
     * @ORM\ManyToOne(targetEntity="Campaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     * })
     */
    private $campaign;

    /**
     * @var \Company
     * @Groups({"integrations"})
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \ConfigurationTargets
     * @Groups({"integrations"})
     * @ORM\ManyToOne(targetEntity="ConfigurationTargets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="configuration_target_id", referencedColumnName="id")
     * })
     */
    private $configurationTarget;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getConfigurationTarget(): ?ConfigurationTargets
    {
        return $this->configurationTarget;
    }

    public function setConfigurationTarget(?ConfigurationTargets $configurationTarget): self
    {
        $this->configurationTarget = $configurationTarget;

        return $this;
    }


}
